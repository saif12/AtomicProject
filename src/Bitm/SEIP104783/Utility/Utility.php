<?php
namespace App\Bitm\SEIP104783\Utility;
class Utility {
    
    
    static public function d($param=false){
        echo "<pre>";
            var_dump($param);
        echo "<pre>";
    }
    static public function dd($param=false){
        self::d($param);
        die();
    }
    static public function redirect($url="Atomic_Project_Rana/views/SEIP104783/Birthday/index.php") {
        header("Location:".$url);
    }
    static public function message($message=null) {
       if(is_null($message)){
           $_message =$_SESSION['message'];
           $_SESSION['message']="";
           return $_message;
       }
       else{
           $_SESSION['message']=$message;
       }
    }
}
