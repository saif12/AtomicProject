-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2016 at 07:38 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `file`
--

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `img_type` varchar(255) NOT NULL,
  `img_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `name`, `img_path`, `img_type`, `img_name`) VALUES
(28, 'Yash', 'files/12274305_952801038127393_6237718378930471051_n.jpg', 'image/jpeg', '12274305_952801038127393_6237718378930471051_n.jpg'),
(29, 'Prosenjit', 'files/1002686_437661799717333_3588640116784527188_n.jpg', 'image/jpeg', '1002686_437661799717333_3588640116784527188_n.jpg'),
(33, 'Saif', 'files/946433_1362374877122108_8072830830180788627_n.jpg', 'image/jpeg', '946433_1362374877122108_8072830830180788627_n.jpg'),
(37, 'Saif', 'files/IMG_1070.jpg', 'image/jpeg', 'IMG_1070.jpg'),
(40, 'jibon', 'files/Penguins.jpg', 'image/jpeg', 'Penguins.jpg'),
(54, 'Saiful Islam', 'files/saiful.jpg', 'image/jpeg', 'saiful.jpg'),
(56, 'Saiful Islam', 'files/IMG_1071.JPG', '', 'IMG_1071.JPG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
