<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Summary_of_organaization\Textarea;
use App\Bitm\SEIP104783\Utility\Utility;

$obj=new Textarea();
$objects =$obj->index();
?>


<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.abcd{color:blue;}
        .idb{background: yellow;color: red;font-size: 25px}
        .delete{margin-top:-25px;margin-left: 35px; float:right}
  </style>
  </head>
  <body>
    <div class="container">
		<h1><span class="glyphicon glyphicon-gift">Summary List</span></h1><hr/>
		<a href="create.php" class="text-right"><h4>Add summary</h4></a>
                <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
		<table class="table table-bordered text-center">
		  <tr class="abcd">
		  	<td class="abc">SL No</td>
		  	<td>Organaization Name</td>
		  	<td>Summary of Organaization </td>
		  	<td>Action</td>
		  </tr>
                  
                  <div class="idb">
                      <?php echo Utility::message();?>
                  </div>
                  
                  <?php
                  $slno=1;
                  foreach($objects as $org){                
                  ?>
		  <tr>
		  	<td><?php echo $org->id; ?></td>
		  	<td><?php echo $org->title; ?></td>
		  	<td><?php echo $org->summary;?></td>
                        <td><a href="show.php?id=<?php echo $org->id?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?php echo $org->id?>">Edit</a>&nbsp; 
                            
                            <form action="delete.php" method="post">
                                <a href="delete.php?id=<?php echo $org->id?>" type="button" class="btn btn-primary delete">Delete</a>
                            </form>
                                
                        </td>  
                            
                                
                            
		  </tr>
                  <?php
                  $slno++;
                  }
                  ?>
		</table>
	</div>

      <script src="../../../Resource/js/bootstrap.min.js"></script>
      <script src="../../../Resource/js/jquery-1.11.3.min.js"></script>
      <script>
          $('.delete').bind('click',function(e){
              var item =confirm("Are you sure you want to delete?");
              if(!item){
                e.preventDefault();
            }
          })
      </script>
    
  </body>
</html>




