<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Profile_Picture\File;
use App\Bitm\SEIP104783\Utility\Utility;


$obj=new File();
$new =$obj->show($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saif</title>
    <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
      <div class="container">
         <h1>Edit Profile Picture</h1><br/>

         <h2>File or Image Editing form</h2><hr/>


         <form action="update.php?id=<?= $new->id?>" method="post" enctype="multipart/form-data">
          <div class="form-inline">
              <input type="text" name="id" class="form-control" value="<?= $new->id; ?>">
          </div>
             
          <div class="form-inline">
            <label for="exampleInputEmail1">User Name</label>
            <input type="text" name="name" class="form-control" value="<?= $new->name; ?>">
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Edit Picture</label>
            <input type="file" name="img_name"/>
            <img src="<?php echo $new->img_path;?>" alt="Rana" height="300px" width="280px">
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form> 
         <a href="index.php">Go To List</a><br>
	 <a href="javascript:history.go(-1)">Back</a>
      </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>
