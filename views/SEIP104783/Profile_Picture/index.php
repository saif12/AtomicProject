<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Profile_Picture\File;
use App\Bitm\SEIP104783\Utility\Utility;

$obj=new File();
$results = $obj->index();
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.abcd{color:blue;}
        .idb{background: green;color:white;font-size: 25px;}
        .deletee{margin-top:-25px; float:right}
        #abcf{background:red;color: white;}
  </style>
  </head>
  <body>
    <div class="container">
        
       
        
	<h1><span class="glyphicon glyphicon-camera"> List of Profile picture</span></h1><hr/>
        
	<a href="create.php" class="text-right"><h4>Add photo</h4></a>
        <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
	<table class="table table-bordered text-center">
            <tr class="abcd">
		<td>ID</td>
		<td>Name</td>
		<td>Image</td>
                <td>Type</td>
                <td>Image Name</td>
                <td>Action</td>
            </tr>
                  
            <div class="idb">
                <?php echo Utility::message()?>
            </div>
           
            <?php
            $slno=1;
            foreach ($results as $abc){
            ?>
		  <tr>
		  	<td><?= $abc->id;?></td>
		  	<td><?= $abc->name; ?></td>
                        <td><img src="<?= $abc->img_path;?>" height="100px" width="120px"/></td>
                        <td><?= $abc->img_type; ?></td>
                        <td><?= $abc->img_name; ?></td>
                        
                        <td>
                            <a href="show.php?id=<?= $abc->id;?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?= $abc->id;?>">Edit</a>&nbsp; 
                            <form action="delete.php" method="post">
                                <a href="delete.php?id=<?= $abc->id?>" type="button" class="btn btn-primary delete">Delete</a>
                                <input type="hidden" name ="id" value="">
                            </form>
                        </td>
		  </tr>
              <?php
              $slno++;
              }
              ?>
		</table>
	</div>

    

    <script src="../../../Resource/js/bootstrap.min.js" ></script>
    <script src="../../../Resource/js/jquery-1.11.3.min.js" ></script>
     <script>
        $('.delete').bind('click',function(e){

            var item = confirm('are you sure you want to delete?');
            if (!item){
                //return false;
                e.preventDefault();
            }
        });


    </script>
  </body>
</html>
