<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saif</title>
    <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
      <div class="container">
         <h1>Add Profile Picture</h1><br/>

         <h2>File or Image uploading form</h2><hr/>


         <form action="store.php" method="post" enctype="multipart/form-data">
          <div class="form-inline">
            <label for="exampleInputEmail1">User Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Name">
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Choice Picture</label>
            <input type="file" name="img_name" id="exampleInputFile">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form> 
      </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>