<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Checkbox_Multiple\Hobby;
use App\Bitm\SEIP104783\Utility\Utility;

$obj = new Hobby;
$new =$obj->show($_REQUEST['id']);
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Favorite Hobby</title>
    <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
  </head>
  
  <style>
      .abc{font-size: 20px}
  </style>
  
  <body>
      <div class="container col-md-4 col-md-offset-4 abc text-primary">
    <h1>Add Your Hobby</h1><hr/>
    <form action="update.php?id=<?php echo $new->id?>" method="post" class="form-inline">
        <label>Edit Your Name</label>
        <input type="hidden" name="id" value="<?php echo $new->id?>"/>
        <input type="text" name="name" class="form-control" value="<?php echo $new->name?>"/><br/><hr/>
       
        <div class="checkbox">
        <label>
            <input type="checkbox" name="sports[]" value="Cricket"> Cricket
        </label>
      </div><br/>
      
       <div class="checkbox">
        <label>
            <input type="checkbox" name="sports[]" value="Football"> Football
        </label>
      </div><br/>
      
       <div class="checkbox">
        <label>
            <input type="checkbox" name="sports[]" value="Badminton"> Badminton
        </label>
      </div><br/>
       
       <div class="checkbox">
        <label>
          <input type="checkbox" name="media[]" value="Acting"> Acting
        </label>
      </div><br/>
       
       <div class="checkbox">
        <label>
          <input type="checkbox" name="media[]" value="Modeling"> Modeling
        </label>
      </div><br/>
      
      <div class="checkbox">
        <label>
          <input type="checkbox" name="media[]" value="Music Video"> Music Video
        </label>
      </div><br/>
       
      <div class="checkbox">
        <label>
          <input type="checkbox" name="profession[]" value="Businessman"> Businessman 
        </label>
      </div><br/>
      
      <div class="checkbox">
        <label>
          <input type="checkbox" name="profession[]" value="Doctor"> Doctor
        </label>
      </div><br/>
      
      <div class="checkbox">
        <label>
          <input type="checkbox" name="profession[]" value="Engineer"> Engineer
        </label>
      </div><br/>
      
      <div class="checkbox">
        <label>
          <input type="checkbox" name="profession[]" value="Banker"> Banker
        </label>
      </div><br/><hr/>
      
      <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    
    <a type="button" href="index.php">Go to list</a><br/>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>



