<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Checkbox_Multiple\Hobby;
use App\Bitm\SEIP104783\Utility\Utility;

$obj = new Hobby();
$new=$obj->index();
?>


<!DOCTYPE html>
<html>
	<head>
		<title>Atomic Project</title>
		<link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
        </head>
        <style>
            .idb{background: green;color:white;font-size: 25px;}
            .abcd{font-weight: bold;color: red;font-size: 18px};
        </style>
        
<body>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    <h1><span class="glyphicon glyphicon-gift"> List of Hobby</span></h1><hr/>
                    <form class="navbar-form navbar-left" role="search">
                       <div class="form-group">
                         <input type="text" class="form-control" placeholder="Type any Search">
                       </div>
                       <button type="submit" class="btn btn-info">Search</button>
                     </form>         
                   <a href="create.php" class="text-right"><h4>Add Hobby</h4></a>
                       <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
        <table class="table table-bordered text-center text-primary">
            <tr class="abcd">
		<td class="abc">ID</td>
		<td>Name</td>
		<td>Favorite Hobby</td>
                <td>Favorite Profession</td>
                <td>Favorite Media</td>
		<td>Action</td>
            </tr>
                  
            <div class="idb">
                <?php echo Utility::message()?>
            </div>
            <div>
                <?php echo Utility::message("Welcome to My Atomic Project")?>
            </div> 
            
                  <?php
                  
                  foreach($new as $abc){                
                  ?>
                  <tr>
		  	<td><?php echo $abc->id;?></td>
		  	<td><?php echo $abc->name; ?></td>
		  	<td><?php echo $abc->sports;?></td>
                        <td><?php echo $abc->profession;?></td>
                        <td><?php echo $abc->media;?></td>
                        
                        <td>
                            <a href="show.php?id=<?php echo $abc->id;?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?php echo $abc->id;?>">Edit</a>&nbsp; 
                            <a href="delete.php?id=<?php echo $abc->id;?>" type="button" class="btn btn-primary delete">Delete</a>
                            <input type="hidden" name ="id" value="<?php echo $abc->ID;?>">
                        </td>
		  </tr>
                  <?php
                  }
                  ?>
		</table>
              </div>
          </div>
		<a href="create.php">Go To List</a><br>
		<a href="javascript:history.go(-1)">Back</a>
		</div>
            <script src="../../../Resource/js/bootstrap.min.js" ></script>
            <script src="../../../Resource/js/jquery-1.11.3.min.js" ></script>
            <script>
                $('.delete').bind('click', function(r){
                    
                       var item= confirm("Are you sure?"); 
                       if(!item){
                       e.preventDefault();
                    }
                    
                })
            </script> 
            
	</body>
</html>

