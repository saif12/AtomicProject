<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Book_Title\Book;
use App\Bitm\SEIP104783\Utility\Utility;
$page=new Book;
$book= $page->index();

?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.abcd{color:blue;}
        .idb{background: green;color:white}
        .delete{margin-top:-25px; float:right}
  </style>
  </head>
  <body>
    <div class="container">
		<h1><span class="glyphicon glyphicon-gift text-n"> List of Book Title</span></h1><hr/>
		<a href="create.php" class="text-right"><h4>Add book title</h4></a>
                <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
		<table class="table table-bordered text-center">
		  <tr class="abcd">
		  	<td class="abc">SL No</td>
		  	<td>ID</td>
		  	<td>Title</td>
                        <td>Author</td>
		  	<td>Action</td>
		  </tr>
                  
                  <div class="idb">
                      <?php echo Utility::message();?>
                  </div>
                  
                      
                  <?php 
                  $slno=1;
                  foreach($book as $bitm){
                  ?>
		  <tr>
		  	<td><?php echo $slno;?></td>
		  	<td><?php echo $bitm->id ;?></td>
		  	<td><?php echo $bitm->title;?></td>
                        <td><?php echo $bitm->author;?></td>
			<td>
                            <a href="show.php?id=<?php echo $bitm->id ;?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?php echo $bitm->id ;?>">Edit</a>&nbsp; 
                             <form action="delete.php" method="post">
                                 <a href="delete.php?id=<?php echo $bitm->id;?>" type="button" class="btn btn-primary delete">Delete</a>
                                <input type="hidden" name ="id" value="<?php echo $bitm->id;?>">
                            </form>
                        </td>
                  </tr>
                  <?php
                  $slno++;
                  }
                  ?>
		</table>
	</div>

    
    <script src="../../../Resource/js/bootstrap.min.js" ></script>
    <script src="../../../Resource/js/jquery-1.11.3.min.js" ></script>
            <script>
                $('.delete').bind('click',function(e){
                    
                    var item = confirm('are you sure you want to delete?');
                    if (!item){
                        //return false;
                        e.preventDefault();
                    }
                });
                
                
            </script>
  </body>
</html>

