<?php

include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."Atomic_Project_Rana".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."startup.php");
use App\Bitm\SEIP104783\Checkbox_Single\Terms;
use App\Bitm\SEIP104783\Utility\Utility;

$obj =new Terms();
$new=$obj->index();
?>



<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.abcd{color:blue;}
        .idb{background: green;color:white;font-size: 25px;}
        #abcf{background:red;color: white;}
  </style>
  </head>
  <body>
    <div class="container">
        
       
        
	<h1><span class="glyphicon glyphicon-gift"> List of Details</span></h1><hr/>
        
        <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Type any Search">
        </div>
        <button type="submit" class="btn btn-info">Search</button>
      </form>
        
	<a href="create.php" class="text-right"><h4>Add purchase</h4></a>
        <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
	<table class="table table-bordered text-center">
            <tr class="abcd">
		<td class="abc">ID</td>
		<td>Name</td>
		<td>Processing</td>
		<td>Action</td>
            </tr>
                  
            <div class="idb">
                <?php echo Utility::message()?>
            </div>
            <div>
                <?php echo Utility::message("Welcome to My Atomic Project")?>
            </div> 
            
                  <?php
                  
                  foreach($new as $abc){                
                  ?>
		  <tr>
		  	<td><?php echo $abc->id;?></td>
		  	<td><?php echo $abc->name; ?></td>
		  	<td><?php echo $abc->processing;?></td>
                        
                        <td>
                            <a href="show.php?id=<?php echo $abc->id;?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?php echo $abc->id;?>">Edit</a>&nbsp; 
                            <a href="delete.php?id=<?php echo $abc->id;?>" type="button" class="btn btn-primary delete">Delete</a>
                            <input type="hidden" name ="id" value="<?php echo $abc->ID;?>">
                        </td>
		  </tr>
                  <?php
                  }
                  ?>
		</table>
	</div>

    

    <script src="../../../Resource/js/bootstrap.min.js" ></script>
    <script src="../../../Resource/js/jquery-1.11.3.min.js" ></script>
            <script>
                $('.delete').bind('click',function(e){
                    
                    var item = confirm('are you sure you want to delete?');
                    if (!item){
                        //return false;
                        e.preventDefault();
                    }
                });
                
                
            </script>
  </body>
</html>

