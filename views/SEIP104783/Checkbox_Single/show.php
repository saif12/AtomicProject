<?php

include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."Atomic_Project_Rana".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."startup.php");
use App\Bitm\SEIP104783\Checkbox_Single\Terms;
use App\Bitm\SEIP104783\Utility\Utility;

$obj =new Terms();
$new=$obj->show($_REQUEST['id']);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Single Checkbox</title>
    <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
  </head>
  
  <style>
      .abc{font-size: 20px}
      .text{color:red}
  </style>
  
  <body>
      <div class="container col-md-4 col-md-offset-4 abc text-success">
        <h1 class="text-danger">Purchase Details</h1>
        <p>Name: <?php echo $new->name;?></p>
        <p class="text">Processing:Your purchase is submitted to database and recently you will get a code number and complete your shopping. </p>
      
        <a href="index.php">Go to List</a><br>
        <a href="javascript:history.go(-1)">Back</a>
      </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>

