<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic_Project_Rana'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Bitm\SEIP104783\Email\Subscription;
use App\Bitm\SEIP104783\Utility\Utility;

$obj=new Subscription;
$myemail= $obj->index();
?>



<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.abcd{color:blue;}
        .idb{background: yellow;color: red}
        .delete{margin-top:-25px; float:right}
  </style>
  </head>
  <body>
    <div class="container">
		<h1><span class="glyphicon glyphicon-gift"> Email List</span></h1><hr/>
		<a href="create.php" class="text-right"><h4>Add New Email</h4></a>
                <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
		<table class="table table-bordered text-center">
		  <tr class="abcd">
		  	<td class="abc">ID</td>
		  	<td>Name</td>
		  	<td>Email</td>
		  	<td>Action</td>
		  </tr>
		  
                  <div class="idb">
                      <?php echo Utility::message();?>
                  </div>
                  
                  <?php
                  foreach ($myemail as $mail){
                  ?>
                    <tr>
		  	<td><?php echo $mail->id?></td>
		  	<td><?php echo $mail->Name;?></td>
		  	<td><?php echo $mail->Email;?></td>
                        <td><a href="show.php?id=<?php echo $mail->id?>">View</a> &nbsp; 
                            <a href="edit.php?id=<?php echo $mail->id?>">Edit</a>&nbsp; 
                            
                            <form action="delete.php" method="post">
                                <a href="delete.php?id=<?php echo $mail->id?>" class="btn btn-primary delete">Delete</a>
                            </form>
                               
                        </td>   
                    </tr>
                  <?php
                  }
                  ?>
		</table>
	</div>

    
    <script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
  </body>
</html>
