<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."Atomic_Project_Rana".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."startup.php");
use App\Bitm\SEIP104783\Select\City;
use App\Bitm\SEIP104783\Utility\Utility;

$obj=new City();
$city=$obj->index();

?>


<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>List</title>
	<link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
  
  
  <style>
	.abc{width:100px}
	.delete{margin-top:-25px; float:right}
        .idb{background: green;color:white;font-size: 25px;}
        .deletes{background:green;color: white;font-size: 20px}
  </style>
  </head>
  <body>
    <div class="container">
        <h1><span class="glyphicon glyphicon-gift"> City Information</span></h1><hr/>
        <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Type any Search">
            </div>
            <button type="submit" class="btn btn-info">Search</button>
      </form>
        
	<a href="create.php" class="text-right"><h4>Add City</h4></a>
        <a href="../../../index.html" class="text-right"><h4>Go to Home</h4></a>
	<table class="table table-bordered text-center">
            <tr class="abcd">
		<td class="abc">SL No</td>
		<td>City Name</td>
		<td>Postal Code</td>
		<td>Action</td>
            </tr> 
            <div class="deletes">
                <?php echo Utility::message();?>
            </div>
            <?php
            foreach ($city as $new){
            ?>
            
            <tr>
		<td><?php echo $new->id?></td>
		<td><?php echo $new->title?></td>
		<td><?php echo $new->postal?></td>
                        
                <td>
                    <a href="show.php?id=<?php echo $new->id?>">View</a> &nbsp; 
                    <a href="edit.php?id=<?php echo $new->id?>">Edit</a>&nbsp; 
                    <form action="delete.php" method="post">
                        <a href="delete.php?id=<?php echo $new->id?>" type="button" class="btn btn-primary delete">Delete</a>
                    <input type="hidden" name ="id">
                    </form>
                </td>
            </tr>
            <?php
            }
            ?>
            
	</table>
    </div>

    

    <script src="../../../Resource/js/bootstrap.min.js" ></script>
    <script src="../../../Resource/js/jquery-1.11.3.min.js" ></script>
    <script>
        $('.delete').bind('click',function(e){
            var rana= confirm("Are you sure you want to delete?");
            if(!rana){
                e.preventDefault();
            }
        })
    </script>
  </body>
</html>
