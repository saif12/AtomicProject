<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."Atomic_Project_Rana".DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."startup.php");
use App\Bitm\SEIP104783\Select\City;
use App\Bitm\SEIP104783\Utility\Utility;

$obj=new City();
$new=$obj->show($_REQUEST['id']);

?>

<h1>City Details</h1>
<table>
    <dl>
        <dt>SL No</dt>
        <dd><?php echo $new->id?></dd>
    </dl>
    <dl>
        <dt>City Name</dt>
        <dd><?php echo $new->title?></dd>
    </dl>
    <dl>
        <dt>Postal Code</dt>
        <dd><?php echo $new->postal?></dd>
    </dl>
</table>

<a href="index.php">Go to list</a><br/>
<a href="../../../index.html">Go to Home</a>


